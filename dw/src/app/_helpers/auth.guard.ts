import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../_services/auth.service';

/**
 * Can activate guard
 * Used to check if user is logged in
 * If user is logged in gaurd returns `true` and load specific path (component). If not, guard redirects to `LoginComponent` with return page as URL param.
 */
@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private authService: AuthService) {}

  /**
   * Main function to check if user is logged in to the Application
   * @param route {ActivatedRouteSnapshot} Activated Route Snapshot
   * @param state {RouterStateSnapshot} Router State Snapshor
   */
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    // Check if user is logged in
    const user = this.authService.getCurrentUser();
    if (user) {
      return true;
    }

    // If not, redirect to `/login?return=LastUrl`
    this.router.navigate(['/login'], { queryParams: { return: state.url } });
    return false;
  }
}
