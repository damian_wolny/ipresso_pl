import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthService } from '../_services/auth.service';

/**
 * Dummy Error Interceptor
 * When API returns 401 logout user
 * Using real backend, in this interceptor we should refresh token.
 * Because in this task API is mocked, refresh token is missed.
 */
@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError((err) => {
        // If 401, logout user and reload page
        if (err.status === 401) {
          this.authService.logout();
          window.location.reload();
        }

        const error = err.error.message || err.statusText;
        return throwError(error);
      }),
    );
  }
}
