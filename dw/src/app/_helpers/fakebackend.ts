import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';
import { mockUsersData } from './fakeData';
import { UserElement } from '../_interfaces/user-element';

const users = [{ id: 1, username: 'admin', password: 'password', firstName: 'Test', lastName: 'Task' }];

let mockUsers = [...mockUsersData];
/**
 * Fake Backend Interceptor
 * Based on the jasonwatmore.com tutorial
 */

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const { url, method, headers, body } = request;

    return of(null).pipe(mergeMap(handleRoute)).pipe(materialize()).pipe(delay(500)).pipe(dematerialize());

    function handleRoute() {
      switch (true) {
        case url.endsWith('/login') && method === 'POST':
          return authenticate();
        case url.endsWith('/users') && method === 'GET':
          return getUsers();
        case url.endsWith('/users') && method === 'POST':
          return addUser(request.body);
        case url.endsWith('/users') && method === 'PATCH':
          return updateUser(request.body);
        case url.endsWith('/users/types') && method === 'GET':
          return getTypes();
        default:
          return next.handle(request);
      }
    }

    function authenticate() {
      const { username, password } = body;
      const user = users.find((x) => x.username === username && x.password === password);
      if (!user) {
        return error('Username or password is incorrect');
      }
      return ok({
        id: user.id,
        username: user.username,
        firstName: user.firstName,
        lastName: user.lastName,
        token: 'fake-jwt-token',
      });
    }

    function updateUser(body) {
      const user: UserElement = { ...body.data };
      const index = mockUsers.findIndex((x) => x.id === user.id);
      mockUsers[index] = user;
      return ok({ data: user });
    }

    function addUser(body) {
      const newUser: UserElement = { ...body.data };
      newUser.id = mockUsers.length + 1;
      mockUsers.push(newUser);
      return ok({ data: newUser });
    }

    function getUsers() {
      if (!isLoggedIn()) {
        return unauthorized();
      }
      return ok({ data: [...mockUsers] });
    }

    function getTypes() {
      const res = {
        data: {
          finance: ['bank', 'insurance'],
          media: ['tv', 'radio'],
          travel: ['national', 'foreign'],
        },
      };

      return ok(res);
    }

    function ok(body?) {
      return of(new HttpResponse({ status: 200, body }));
    }

    function error(message) {
      return throwError({ error: { message } });
    }

    function unauthorized() {
      return throwError({ status: 401, error: { message: 'Unauthorised' } });
    }

    function isLoggedIn() {
      return headers.get('Authorization') === 'Bearer fake-jwt-token';
    }
  }
}

export let fakeBackendProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: FakeBackendInterceptor,
  multi: true,
};
