import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AuthService } from '../_services/auth.service';

/**
 * JWT Interceptor
 * Used to add Bearer token to requests to API
 */

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) {}

  /**
   * Main Interceptor for API requests.
   * Check if user is logged into the Application. Next, check if user got token.
   * If yes, and request URL is to the API (indexOf check) add token to headers.
   * @param request {HttpRequest} Http Request
   * @param next {HttpHandler} HTTP Handler
   */
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const user = this.authService.getCurrentUser();
    const isLogged = user && user.token;
    const isApiUrl = request.url.indexOf(environment.APIURL) > -1;
    if (isLogged && isApiUrl) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${user.token}`,
        },
      });
    }

    return next.handle(request);
  }
}
