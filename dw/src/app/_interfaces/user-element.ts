/**
 * User element interface
 */
export interface UserElement {
  id: number;
  first_name: string;
  last_name: string;
  email: string;
  birthday: string;
  phone: string;
  type: string;
  subtype: string;
}
