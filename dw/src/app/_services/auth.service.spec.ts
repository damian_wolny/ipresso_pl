import { TestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { AUTH } from './../__consts';
import { User } from '../classes/user';
import { environment } from 'src/environments/environment';

describe('AuthService', () => {
  let service: AuthService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(AuthService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should handle login', () => {
    const expectUser: User = {
      id: 1,
      username: 'admin',
      firstName: 'Test',
      lastName: 'Task',
      token: 'fake-jwt-token',
    };

    service.login('admin', 'password').subscribe((user: User | null) => {
      const u = localStorage.getItem(AUTH.USER_LOCALSTORAGE_KEY);
      expect(user).toEqual(expectUser);
      expect(u).toEqual(JSON.stringify(user));
    });
    httpMock.expectOne(`${environment.APIURL}/login`).flush(expectUser);
  });

  it('logout() should work correctly', () => {
    localStorage.setItem(AUTH.USER_LOCALSTORAGE_KEY, 'test');
    service.logout();
    expect(localStorage.getItem(AUTH.USER_LOCALSTORAGE_KEY)).toBeNull();
    service.userSubject.subscribe((user: User | null) => {
      expect(user).toBeNull();
    });
  });
});
