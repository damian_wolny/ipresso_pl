import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { User } from '../classes/user';
import { AUTH } from './../__consts';

/**
 * Authentication Service
 * Main service to handle with user, e.g. login to app or return User object.
 */
@Injectable({
  providedIn: 'root',
})
export class AuthService {
  /**
   * BehaviorSubject with user.
   * Used to check if user is logged in and to get user inside components
   */
  public userSubject: BehaviorSubject<User>;

  /**
   * Observable variable with User
   */
  public user: Observable<User>;

  constructor(private http: HttpClient) {
    this.userSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem(AUTH.USER_LOCALSTORAGE_KEY)));
    this.user = this.userSubject.asObservable();
  }

  /**
   * Return user. Used in AuthGuard to check if user in the app exists (if user is logged in)
   */
  getCurrentUser(): User {
    return this.userSubject.value;
  }

  /**
   * Dummy login function. Used in LoginComponent to login user in the app.
   * @param username
   * @param password
   */
  login(username: string, password: string): Observable<any> {
    return this.http.post(`${environment.APIURL}/login`, { username, password }).pipe(
      map((user: User | null) => {
        localStorage.setItem(AUTH.USER_LOCALSTORAGE_KEY, JSON.stringify(user));
        this.userSubject.next(user);
        return user;
      }),
    );
  }

  /**
   * Logout user
   */
  logout(): void {
    localStorage.removeItem(AUTH.USER_LOCALSTORAGE_KEY);
    this.userSubject.next(null);
  }
}
