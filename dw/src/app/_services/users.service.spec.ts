import { TestBed } from '@angular/core/testing';

import { UsersService } from './users.service';

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { environment } from 'src/environments/environment';

describe('UsersService', () => {
  let service: UsersService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(UsersService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getUsers() should returned data', () => {
    const expectedUsers = [
      {
        id: 1,
        first_name: 'Lenore',
        last_name: 'Brock',
        email: 'sapien@Sedauctor.edu',
        phone: '378278396',
        birthday: '1963-08-04',
        type: 'media',
        subtype: 'radio',
      },
      {
        id: 2,
        first_name: 'Celeste',
        last_name: 'Oneal',
        email: 'aliquam.iaculis.lacus@euelit.org',
        phone: '954613453',
        birthday: '1982-02-24',
        type: 'finance',
        subtype: 'bank',
      },
      {
        id: 3,
        first_name: 'Quin',
        last_name: 'Mason',
        email: 'parturient.montes.nascetur@mipede.edu',
        phone: '365165095',
        birthday: '1969-01-04',
        type: 'media',
        subtype: 'tv',
      },
    ];

    service.getUsers().subscribe((res) => {
      expect(res).toEqual(expectedUsers);
    });

    httpMock.expectOne(`${environment.APIURL}/users`).flush(expectedUsers);
  });

  it('getTypes() should set service`s variables', () => {
    const expectedTypes = {
      data: {
        finance: ['bank', 'insurance'],
        media: ['tv', 'radio'],
        travel: ['national', 'foreign'],
      },
    };

    service.getTypes().subscribe((res: any) => {
      expect(service.subtypes).toEqual(res.data);
    });

    httpMock.expectOne(`${environment.APIURL}/users/types`).flush(expectedTypes);
  });
});
