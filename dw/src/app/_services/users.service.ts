import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { UserElement } from '../_interfaces/user-element';

/**
 * Users Service
 */
@Injectable({
  providedIn: 'root',
})
export class UsersService {
  public subtypes = {};
  constructor(private http: HttpClient) {}

  getUsers(): Observable<any> {
    return this.http.get(`${environment.APIURL}/users`);
  }

  getTypes(): Observable<any> {
    return this.http.get(`${environment.APIURL}/users/types`).pipe(
      map((types: any) => {
        this.subtypes = { ...types.data };
        return types;
      }),
    );
  }

  addUser(data: UserElement): Observable<any> {
    return this.http.post(`${environment.APIURL}/users`, { data });
  }

  updateUser(data: UserElement): Observable<any> {
    return this.http.patch(`${environment.APIURL}/users`, { data });
  }
}
