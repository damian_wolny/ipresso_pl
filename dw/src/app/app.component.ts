import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  constructor(translate: TranslateService, private router: Router) {
    translate.setDefaultLang('pl');
    translate.use('pl');
  }

  /**
   * @ignore
   */
  ngOnInit(): void {
    this.router.navigate(['/panel']);
  }
}
