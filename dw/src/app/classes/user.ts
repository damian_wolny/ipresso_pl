/**
 * User class
 * This class describes user properties and internal function
 */

export class User {
  /**
   * User ID
   */
  public id: number = 0;

  /**
   * User's username
   */
  public username: string = '';

  /**
   * User's first name
   */
  public firstName: string = '';

  /**
   * User's last name
   */
  public lastName: string = '';

  /**
   * User's token
   */
  public token: string = '';
}
