import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthService } from '../_services/auth.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { LoginComponent } from './login.component';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { of, throwError } from 'rxjs';
import { Router } from '@angular/router';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let httpMock: HttpTestingController;
  let authService: AuthService;
  const routerSpy = { navigate: jasmine.createSpy('navigate') };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LoginComponent],
      imports: [ReactiveFormsModule, BrowserAnimationsModule, HttpClientTestingModule, RouterTestingModule, MatInputModule, TranslateModule.forRoot()],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [AuthService, { provide: Router, useValue: routerSpy }],
    }).compileComponents();
  });

  afterEach(() => {
    httpMock.verify();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    httpMock = TestBed.inject(HttpTestingController);
    authService = TestBed.inject(AuthService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.login.valid).toBeFalsy();
  });

  it('`username` field valid', () => {
    let err = {};
    const field = component.login.controls.username;
    err = field.errors || {};
    expect(err['required']).toBeTruthy();
    field.setValue('test');
    err = field.errors || {};
    expect(field.valid).toBeTruthy();
  });

  it('`password` field valid', () => {
    let err = {};
    const field = component.login.controls.password;
    err = field.errors || {};
    expect(err['required']).toBeTruthy();
    field.setValue('test');
    err = field.errors || {};
    expect(field.valid).toBeTruthy();
  });

  it('submitLogin() should work correctly', () => {
    component.submitLogin();
    expect(component.loginError).toBeTruthy();
    component.login.controls.username.setValue('test');
    component.login.controls.password.setValue('test');
    spyOn(authService, 'login').and.returnValue(
      of({
        id: 1,
        username: 'username',
        firstName: 'firstName',
        lastName: 'lastName',
        token: 'fake-jwt-token',
      }),
    );
    component.submitLogin();
    expect(routerSpy.navigate).toHaveBeenCalledWith(['/panel']);
  });

  it('submitLogin() should work correctly when error', () => {
    component.login.controls.username.setValue('test');
    component.login.controls.password.setValue('test');
    spyOn(authService, 'login').and.returnValue(throwError(new Error('test')));
    component.submitLogin();
    expect(component.loginError).toBeTruthy();
  });
});
