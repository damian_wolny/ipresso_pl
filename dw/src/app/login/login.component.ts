import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { AuthService } from '../_services/auth.service';

/**
 * Login Component
 * Entry point to the Application.
 * Validate inputs and send request to API
 */
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  /**
   * Form group object
   */
  public login: FormGroup;

  /**
   * Variable used to check if login request return success
   */
  public loginError: boolean = false;

  constructor(private formBuilder: FormBuilder, private authService: AuthService, private router: Router) {}

  /**
   * Init FormBuilder
   */
  ngOnInit(): void {
    this.login = this.formBuilder.group({
      username: [null, Validators.required],
      password: [null, Validators.required],
    });
  }

  /**
   * Called in HTML after click on the login button.
   * Check valid login form. If form is valid, send request to API
   */
  submitLogin(): void {
    if (!this.login.valid) {
      this.loginError = true;
      return;
    }
    this.loginError = false;
    this.authService
      .login(this.login.value.username, this.login.value.password)
      .pipe(take(1))
      .subscribe(
        (res) => {
          // User is logged in
          this.router.navigate(['/panel']);
        },
        (err) => {
          this.loginError = true;
        },
      );
  }
}
