import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateModule } from '@ngx-translate/core';
import { of } from 'rxjs';
import { UserElement } from 'src/app/_interfaces/user-element';
import { UsersService } from 'src/app/_services/users.service';

import { PanelDialogComponent } from './panel-dialog.component';

class MatSnackBarMock {
  open() {
    return {
      onAction: () => of({}),
    };
  }
}

describe('PanelDialogComponent', () => {
  let component: PanelDialogComponent;
  let fixture: ComponentFixture<PanelDialogComponent>;
  let usersService: UsersService;

  const mockTypes = {
    data: {
      finance: ['bank', 'insurance'],
      media: ['tv', 'radio'],
      travel: ['national', 'foreign'],
    },
  };

  const mockUser: UserElement = {
    id: 0,
    first_name: '',
    last_name: '',
    email: '',
    birthday: '',
    phone: '',
    type: '',
    subtype: '',
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PanelDialogComponent],
      imports: [
        ReactiveFormsModule,
        BrowserAnimationsModule,
        MatDialogModule,
        MatInputModule,
        MatSelectModule,
        HttpClientTestingModule,
        TranslateModule.forRoot(),
      ],
      providers: [
        UsersService,
        { provide: MatDialogRef, useValue: { close: (args?) => {} } },
        { provide: MAT_DIALOG_DATA, useValue: { user: mockUser } },
        { provide: MatSnackBar, useClass: MatSnackBarMock },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    usersService = TestBed.inject(UsersService);
    spyOn(usersService, 'getTypes').and.returnValue(of(mockTypes));
    fixture = TestBed.createComponent(PanelDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.form.valid).toBeFalsy();
    expect(component.types).toEqual(Object.keys(mockTypes.data));
  });

  it('`first_name` field validity', () => {
    let err = {};
    const field = component.form.controls.first_name;
    err = field.errors || {};
    expect(field.valid).toBeFalsy();
    expect(err['required']).toBeTruthy();
    field.setValue('John');
    expect(field.valid).toBeTruthy();
  });

  it('`last_name` field validity', () => {
    let err = {};
    const field = component.form.controls.last_name;
    err = field.errors || {};
    expect(field.valid).toBeFalsy();
    expect(err['required']).toBeTruthy();
    field.setValue('Doe');
    expect(field.valid).toBeTruthy();
  });

  it('`email` field validity', () => {
    let err = {};
    const field = component.form.controls.email;
    err = field.errors || {};

    // Empty value
    expect(field.valid).toBeFalsy();
    expect(err['required']).toBeTruthy();

    // Invalid pattern value
    field.setValue('testemail');
    err = field.errors || {};
    expect(field.valid).toBeFalsy();
    expect(err['required']).toBeFalsy();
    expect(err['pattern']).toBeTruthy();

    // Valid value
    field.setValue('test@test.pl');
    err = field.errors || {};
    expect(field.valid).toBeTruthy();
    expect(err['required']).toBeFalsy();
    expect(err['pattern']).toBeFalsy();
  });

  it('`birthday` field validity', () => {
    let err = {};
    const field = component.form.controls.birthday;
    err = field.errors || {};
    expect(field.valid).toBeFalsy();
    expect(err['required']).toBeTruthy();

    field.setValue(new Date(1991, 10, 15));
    err = field.errors || {};
    expect(field.valid).toBeTruthy();

    // Check date condition (18 years old or older)
    const date: Date = field.value;
    expect(component.maxDate.getTime() / 1000).toBeGreaterThanOrEqual(date.getTime() / 1000);
  });

  it('`phone` field validity', () => {
    let err = {};
    const field = component.form.controls.phone;
    err = field.errors || {};
    expect(field.valid).toBeFalsy();
    expect(err['required']).toBeTruthy();

    // Check patterns
    field.setValue('test');
    err = field.errors || {};
    expect(field.valid).toBeFalsy();
    expect(err['pattern']).toBeTruthy();

    // Invalid length
    field.setValue('0123456');
    err = field.errors || {};
    expect(field.valid).toBeFalsy();
    expect(err['pattern']).toBeTruthy();
    expect(err['minlength']).toBeTruthy();
    field.setValue('01234567890');
    err = field.errors || {};
    expect(err['minlength']).toBeFalsy();
    expect(err['maxlength']).toBeTruthy();

    // Valid length, invalid firstCharacterInPhone
    field.setValue('012345678');
    err = field.errors || {};
    expect(field.valid).toBeFalsy();
    expect(err['pattern']).toBeFalsy();
    expect(err['minlength']).toBeFalsy();
    expect(err['maxlength']).toBeFalsy();
    expect(err['invalidFirstCharacter']).toBeTruthy();

    // Valid field
    field.setValue('600700800');
    expect(field.valid).toBeTruthy();
  });

  it('`type` field validity', () => {
    let err = {};
    const field = component.form.controls.type;
    err = field.errors || {};
    expect(field.valid).toBeFalsy();
    expect(err['required']).toBeTruthy();

    field.setValue('media');
    expect(field.valid).toBeTruthy();
  });

  it('`subtype` field validity', () => {
    let err = {};
    const field = component.form.controls.subtype;
    err = field.errors || {};
    expect(field.valid).toBeFalsy();
    expect(err['required']).toBeTruthy();

    field.setValue('tv');
    expect(field.valid).toBeTruthy();
  });

  it('should close Dialog or not', () => {
    spyOn(component['dialogRef'], 'close');
    component.save();

    // Valid fields
    component.form.controls.first_name.setValue('John');
    component.form.controls.last_name.setValue('Doe');
    component.form.controls.email.setValue('test@test.pl');
    component.form.controls.birthday.setValue(new Date(1991, 10, 9));
    component.form.controls.phone.setValue('0');
    component.form.controls.type.setValue('media');
    component.form.controls.subtype.setValue('radio');
    component.save();
    expect(component['dialogRef'].close).not.toHaveBeenCalled();

    // Component close() function
    component.close();
    expect(component['dialogRef'].close).toHaveBeenCalled();
  });
});
