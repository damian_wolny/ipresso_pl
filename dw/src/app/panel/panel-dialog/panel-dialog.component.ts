import { Component, Inject, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { take } from 'rxjs/operators';
import { UserElement } from 'src/app/_interfaces/user-element';
import { UsersService } from 'src/app/_services/users.service';

/**
 * Modal Component
 * Used to add or edit user
 */

@Component({
  selector: 'app-panel-dialog',
  templateUrl: './panel-dialog.component.html',
  styleUrls: ['./panel-dialog.component.scss'],
})
export class PanelDialogComponent implements OnInit {
  /**
   * FormGroup instance
   */
  public form: FormGroup;

  /**
   * Component variable to grab User
   */
  public user: UserElement = {
    id: 0,
    first_name: '',
    last_name: '',
    email: '',
    birthday: '',
    phone: '',
    type: '',
    subtype: '',
  };

  /**
   * Maximum date for DatePricekr
   */
  public maxDate: Date;

  /**
   * Option values for `type` select
   */
  public types: Array<string>;

  /**
   * Configure initial data:
   *  - user as UserElement
   *  - form as FormBuilder with validators:
   *    - first_name: required
   *    - last_name: required
   *    - email: required, pattern
   *    - birthday: required, maxDate
   *    - phone: required, minLength, maxLength, pattern, firstCharacterInPhone
   *    - type: required
   *    - subtype: required
   */
  constructor(
    private formBuilder: FormBuilder,
    public usersService: UsersService,
    private dialogRef: MatDialogRef<PanelDialogComponent>,
    private snackBar: MatSnackBar,
    private translate: TranslateService,
    @Inject(MAT_DIALOG_DATA) data,
  ) {
    this.user = data.user;
    this.form = this.formBuilder.group({
      id: [this.user.id],
      first_name: [this.user.first_name, Validators.required],
      last_name: [this.user.last_name, Validators.required],
      email: [this.user.email, [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      birthday: [this.user.birthday, Validators.required],
      phone: [
        this.user.phone,
        [Validators.required, Validators.minLength(9), Validators.maxLength(9), Validators.pattern('[0-9]{9}'), CustomValidator.firstCharacterInPhone],
      ],
      type: [this.user.type, Validators.required],
      subtype: [this.user.subtype, Validators.required],
    });
  }

  /**
   * Get date 18 years back from now
   */
  ngOnInit(): void {
    const now = new Date();
    const currentYear = now.getFullYear();
    const currentMonth = now.getMonth();
    const currentDay = now.getDay();
    this.maxDate = new Date(currentYear - 18, currentMonth, currentDay);
    this.usersService
      .getTypes()
      .pipe(take(1))
      .subscribe(
        (res: any) => {
          this.types = [...Object.keys(res.data)];
        },
        (err) => {
          this.snackBar.open(this.translate.instant('panel.error'), 'x', {
            duration: 5000,
          });
        },
      );
  }

  /**
   * Save (edit) or add user
   */
  save(): void {
    this.form.markAllAsTouched();
    if (this.form.valid) {
      this.dialogRef.close(this.form.value);
    }
  }

  /**
   * Close modal without save
   */
  close(): void {
    this.dialogRef.close(null);
  }
}

/**
 * CustomValidator
 */
export class CustomValidator {
  static firstCharacterInPhone(control: AbstractControl) {
    const val = control.value.toString().charAt(0);

    if (val === null || val === '') {
      return null;
    }

    if (val === '0') {
      return { invalidFirstCharacter: true };
    }

    return null;
  }
}
