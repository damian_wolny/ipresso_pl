import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { of, throwError } from 'rxjs';
import { UserElement } from '../_interfaces/user-element';
import { UsersService } from '../_services/users.service';

import { PanelComponent } from './panel.component';

class MatSnackBarMock {
  open() {
    return {
      onAction: () => of({}),
    };
  }
}

describe('PanelComponent', () => {
  let component: PanelComponent;
  let fixture: ComponentFixture<PanelComponent>;
  let usersService: UsersService;

  const mockUsersData = [
    {
      id: 1,
      first_name: 'Lenore',
      last_name: 'Brock',
      email: 'sapien@sedauctor.edu',
      phone: '378278396',
      birthday: '1963-08-04',
      type: 'media',
      subtype: 'radio',
    },
    {
      id: 2,
      first_name: 'Celeste',
      last_name: 'Oneal',
      email: 'aliquam.iaculis.lacus@euelit.org',
      phone: '954613453',
      birthday: '1982-02-24',
      type: 'finance',
      subtype: 'bank',
    },
    {
      id: 3,
      first_name: 'Quin',
      last_name: 'Mason',
      email: 'parturient.montes.nascetur@mipede.edu',
      phone: '365165095',
      birthday: '1969-01-04',
      type: 'media',
      subtype: 'tv',
    },
  ];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PanelComponent],
      imports: [RouterTestingModule, HttpClientTestingModule, TranslateModule.forRoot(), MatSnackBarModule, MatDialogModule],
      providers: [UsersService, { provide: MatSnackBar, useClass: MatSnackBarMock }],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelComponent);
    component = fixture.componentInstance;
    usersService = TestBed.inject(UsersService);
    fixture.detectChanges();
  });

  it('loadUsers() should create DataTable when HTTP response is success', () => {
    spyOn(usersService, 'getUsers').and.returnValue(of({ data: mockUsersData }));
    component.loadUsers();
    expect(component.users.data.length).toEqual(3);
    expect(component.users.filter.length).toEqual(0);
  });

  it('loadUsers() should call snackBar when HTTP error', () => {
    spyOn(usersService, 'getUsers').and.returnValue(throwError(new Error('test')));
    spyOn(component['snackBar'], 'open').and.callThrough();
    component.loadUsers();
    expect(component['snackBar'].open).toHaveBeenCalled();
  });

  it('editUser with existing user: afterClosed with null', () => {
    let dialogRefSpyObj = jasmine.createSpyObj({ afterClosed: of({}), close: null });
    dialogRefSpyObj.componentInstance = { body: '' };
    const mockUser: UserElement = {
      id: 1,
      first_name: 'Lenore',
      last_name: 'Brock',
      email: 'sapien@sedauctor.edu',
      phone: '378278396',
      birthday: '1963-08-04',
      type: 'media',
      subtype: 'radio',
    };

    let dialogSpy = spyOn(TestBed.inject(MatDialog), 'open').and.returnValue(dialogRefSpyObj);
    component.editUser(mockUser);
    expect(dialogSpy).toHaveBeenCalled();
  });

  it('editUser with existing user: afterClosed with user', () => {
    const mockUser: UserElement = {
      id: 1,
      first_name: 'Lenore',
      last_name: 'Brock',
      email: 'sapien@sedauctor.edu',
      phone: '378278396',
      birthday: '1963-08-04',
      type: 'media',
      subtype: 'radio',
    };
    let dialogRefSpyObj = jasmine.createSpyObj({ afterClosed: of(mockUser), close: mockUser });
    dialogRefSpyObj.componentInstance = { body: '' };

    let dialogSpy = spyOn(TestBed.inject(MatDialog), 'open').and.returnValue(dialogRefSpyObj);
    component.editUser(mockUser);
    expect(dialogSpy).toHaveBeenCalled();
  });

  // I've tried to test it using nativeElement, but at work I use PrimeNG instead of Angular Material
  // So i change function argument from Event to string
  // it('filterTable should set filter on the table', (done) => {
  //   fixture.detectChanges();
  //   fixture.whenStable().then(() => {
  //     const input = fixture.debugElement.query(By.css('#panel-filter')).nativeElement;
  //     input.value = 'Lenore';
  //     component.filterTable(input.dispatchEvent(new Event('input')));
  //     expect(component.users.filter).toEqual('Lenore');
  //     expect(component.users.sortData.length).toEqual(1);
  //     done();
  //   });
  // });

  it('filterTable should set filter on the table', () => {
    spyOn(usersService, 'getUsers').and.returnValue(of({ data: mockUsersData }));
    component.loadUsers();
    fixture.detectChanges();
    expect(component.users.data.length).toEqual(3);
    component.filterTable(' Lenore');
    fixture.detectChanges();
    expect(component.users.filter).toEqual('lenore');
  });
});
