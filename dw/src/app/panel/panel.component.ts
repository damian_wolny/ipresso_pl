import { unsupported } from '@angular/compiler/src/render3/view/util';
import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TranslateService } from '@ngx-translate/core';
import { take } from 'rxjs/operators';
import { UserElement } from '../_interfaces/user-element';
import { UsersService } from '../_services/users.service';
import { PanelDialogComponent } from './panel-dialog/panel-dialog.component';

/**
 * Panel Component
 * Main component used to display users data in the table
 * Also used to edit and delete user data.
 *
 * ChangeDetectionStrategy is set to OnPush for performance on the older browsers
 */
@Component({
  selector: 'app-panel',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.scss'],
})
export class PanelComponent implements AfterViewInit {
  /**
   * ViewChild for MatPaginator
   */
  @ViewChild(MatPaginator) paginator: MatPaginator;

  /**
   * ViewChild for MatSort
   */
  @ViewChild(MatSort) sort: MatSort;

  /**
   * Columns used by Angular Material Table module
   */
  public columns: string[] = ['first_name', 'last_name', 'email', 'phone', 'birthday', 'type', 'subtype', 'actions'];

  /**
   * Main data wrapper for table
   */
  public users: MatTableDataSource<UserElement>;

  /**
   * EmptyUser object
   */
  public emptyUser: UserElement = {
    id: 0,
    first_name: '',
    last_name: '',
    email: '',
    birthday: '',
    phone: '',
    type: '',
    subtype: '',
  };
  constructor(
    private usersService: UsersService,
    private ref: ChangeDetectorRef,
    private snackBar: MatSnackBar,
    private translate: TranslateService,
    private dialog: MatDialog,
  ) {
    this.loadUsers();
  }

  /**
   * @ignore
   */
  ngAfterViewInit(): void {}

  /**
   * Load users from API.
   * After load, set in MatTableDataSource (secured by Interface), set paginator and sort.
   * For improve speed on older browsers detectChanges called manually
   */
  loadUsers(): void {
    this.usersService
      .getUsers()
      .pipe(take(1))
      .subscribe(
        (res: any) => {
          this.users = new MatTableDataSource<UserElement>(res.data);
          this.users.paginator = this.paginator;
          this.users.sort = this.sort;
          this.ref.detectChanges();
        },
        (err) => {
          this.snackBar.open(this.translate.instant('panel.error'), 'x', {
            duration: 5000,
          });
          this.ref.detectChanges();
        },
      );
  }

  /**
   * Default filter table function
   * @param value {string} String value
   */
  filterTable(value: string): void {
    this.users.filter = value.trim().toLowerCase();

    if (this.users.paginator) {
      this.users.paginator.firstPage();
    }
  }

  editUser(user: UserElement): void {
    const dialogCfg = new MatDialogConfig();
    dialogCfg.disableClose = true;
    dialogCfg.autoFocus = true;

    dialogCfg.data = { user };

    const dialogRef = this.dialog.open(PanelDialogComponent, dialogCfg);
    dialogRef.afterClosed().subscribe(
      (u: UserElement | null) => {
        if (u === null) {
          return;
        }
        if (u.id === 0) {
          this.usersService
            .addUser(u)
            .pipe(take(1))
            .subscribe(
              ({ data }) => {
                this.users.data = this.users.data.concat(data);
                this.users.paginator = this.paginator;
                this.users._updateChangeSubscription();
              },
              (err) => {
                this.snackBar.open(this.translate.instant('panel.error'), 'x', {
                  duration: 5000,
                });
                this.ref.detectChanges();
              },
            );
        } else {
          this.usersService
            .updateUser(u)
            .pipe(take(1))
            .subscribe(
              (updatedUser: any) => {
                const index = this.users.data.findIndex((x) => x.id === updatedUser.id);
                this.users.data[index] = { ...updatedUser };
                this.users.paginator = this.paginator;
              },
              (err) => {
                this.snackBar.open(this.translate.instant('panel.error'), 'x', {
                  duration: 5000,
                });
                this.ref.detectChanges();
              },
            );
        }
      },
      (err) => {
        this.snackBar.open(this.translate.instant('panel.error'), 'x', {
          duration: 5000,
        });
        this.ref.detectChanges();
      },
    );
  }
}
