import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PanelComponent } from './panel.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { AngularMaterialModule } from '../_helpers/angular-material.module';
import { PanelDialogComponent } from './panel-dialog/panel-dialog.component';

@NgModule({
  declarations: [PanelComponent, PanelDialogComponent],
  imports: [CommonModule, AngularMaterialModule, TranslateModule, FormsModule, ReactiveFormsModule],
  exports: [PanelComponent, PanelDialogComponent],
})
export class PanelModule {}
